# Truffle React DApp for Migration of ERC20 token

Create a DApp that token holders can send their ERC20 tokens to, to be burned and then
swapped for equivalent token on main net.

Users must create a new wallet on the Polkadot network, specify the wallet address and then approve the DApp to transfer tokens from their Ethereum wallet (to be burned).


## Development Setup Notes

* Prettier seems too opinionated so I've done as best I can to setup eslint so you can disable Prettier package in VSCode.

* Install ESLint vscode plugin and configure "eslint.autoFixOnSave": true in Settings

* NPM task 'build' doesn't build source maps which is what you want in Production mode. If you use the build task and expect to debug the application, remove the --no-source-maps option.

### Quick Start
* compile contracts with
```shell
truffle compile
```
* in a separate terminal, run
```shell
ganache-cli -p 7545 -i 5777
```
* deploy contracts
```shell
truffle migrate --network ganache
```
Our smart contract assigns all the minted tokens to the contract owner so we need to send some to another account from the console:
```shell
truffle --network ganache
let accts = await web3.eth.getAccounts()
let oax = await OAXToken.deployed()
oax.transfer(accts[1], 88)
oax.balanceOf(accts[1])
```

* start the dapp
```shell
yarn start
```


This is a React based app, created using nano and parcel.
