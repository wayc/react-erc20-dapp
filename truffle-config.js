const path = require('path');

module.exports = {
  compilers: {
    solc: {
      version: '0.5.0'
    }
  },
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!
  contracts_build_directory: path.join(__dirname, 'src/contracts'),
  networks: {
    develop: {
      port: 8545,
      network_id: '*'
    },
    ganache: {
      host: '127.0.0.1',
      port: 7545,
      network_id: '*'
    }
  }
};
