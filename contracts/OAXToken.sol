pragma solidity ^0.5.0;

// ----------------------------------------------------------------------------
// Copyright (c) 2018,2019 OAX Foundation.
// https://www.oax.org/
// See LICENSE file for license details.
// ----------------------------------------------------------------------------

// npm openzeppelin-solidity package deprecates need for local OZ contracts
import 'openzeppelin-solidity/contracts/token/ERC20/ERC20.sol';


contract OAXToken is ERC20 {
    string public constant name = 'OAX-Token';
    string public constant symbol = "OAX";
    uint8 public constant decimals = 2;
    uint public INITIAL_SUPPLY = 300000 * (10 ** uint256(decimals));

    constructor() public {
        //_balances[msg.sender] = _totalSupply;
        _mint(msg.sender, INITIAL_SUPPLY);

        emit Transfer(address(0), msg.sender, INITIAL_SUPPLY);
    }
}
