/* eslint-disable no-undef */
/* eslint-disable react/prop-types */
/* eslint-disable react/display-name */
import React from 'react';
import Web3 from 'web3';
import BigNumber from 'bignumber.js';
import OAXContract from './contracts/OAXToken.json';
import getWeb3 from './utils/getWeb3.js';

class App extends React.Component {
  state = { storageValue: 0, web3: null, accounts: null, contract: null };

  componentDidMount = async () => {
    if (window.ethereum) {
      web3 = new Web3(window.ethereum);
      try {
        window.ethereum.enable().then(function () {
          // User has allowed account access to DApp...
        });
      } catch (e) {
        // User has denied account access to DApp...
      }
    } else if (window.web3) {
      web3 = new Web3(window.web3.currentProvider);
    } else {
      alert('You have to install MetaMask');
    }

    try {
      const web3 = await getWeb3();
      const accounts = await web3.eth.getAccounts();
      // Get the contract instance.
      const networkId = await web3.eth.net.getId();
      const deployedNetwork = OAXContract.networks[networkId];
      const oaxContract = new web3.eth.Contract(
        OAXContract.abi,
        deployedNetwork && deployedNetwork.address,
      );

      // Set web3, accounts, and contract to the state, and then proceed with an
      // example of interacting with the contract's methods.
      this.setState(
        { web3, accounts, oaxERC20: oaxContract },
        this.runExample
      );

    } catch (error) {
      // Catch any errors for any of the above operations.
      alert(
        'Failed to load web3, accts, or contract. Check console for details.',
      );
      console.error(error);
    }

  }

  runExample = async () => {
    const { accounts, contract, oaxERC20 } = this.state;
    // Stores a given value, 5 by default.
    //await contract.methods.set(5).send({ from: accounts[0] });

    // Get the value from the contract to prove it worked.
    //const response = await contract.methods.get().call();
    //this.setState({ storageValue: response });

    // below transfer doesn't work because I cannot send between
    // my own multiple wallets due to metamask only return current account
    // when we call getAccounts
    // await oaxERC20.methods.transfer(accounts[2], 888).send({ from: accounts[0] });
    const mybal = await oaxERC20.methods.balanceOf(accounts[0]).call({ from: accounts[0] }, function (error, balance) {
      //console.log(error);
      //console.log(balance);
    });
    let myoax = new BigNumber(mybal);
    const decs = await oaxERC20.methods.decimals().call();
    this.setState({ storageValue: myoax.div(10 ** decs).toNumber() });

  };

  render() {
    if (!this.state.web3) {
      return <div>Loading Web3, accounts, and contract...</div>;
    }
    return (
      <div className="App">
        <h1>
          Welcome to OAX ERC20 Migration Page
        </h1>
        <p>This needs to be replaced with a proper title/welcome page</p>
        <p>
          Try changing the value stored on <strong>line 40</strong> of App.js.
        </p>
        <div>Your OAX balance is: {this.state.storageValue}</div>
      </div>
    );
  }

}

export default App;
